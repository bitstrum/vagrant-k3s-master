# -*- mode: ruby -*-
# vi: set ft=ruby :

K3S_VERSION = 'v0.9.1'
BOX_IP_ADDRESS = '192.168.1.1'
BOX_HOSTNAME = 'k3s-master'
BOX_MEMORY_SIZE = '2048'
BOX_CPU_COUNT = '2'

VAGRANTFILE_API_VERSION = '2'

$script = <<-SCRIPT
	set -e

	K3S_VERSION="$1"
	BOX_IP="$2"

	apt-get update
	apt-get dist-upgrade --yes
	apt-get install --yes curl ca-certificates

	cp /vagrant/conf/00-firewall /etc/network/if-up.d/
	chmod +x /etc/network/if-up.d/00-firewall
	/etc/network/if-up.d/00-firewall

	cp /vagrant/conf/bash_aliases /home/vagrant/.bash_aliases
	chown vagrant:vagrant /home/vagrant/.bash_aliases

	curl -sSL https://get.k3s.io | INSTALL_K3S_VERSION="${K3S_VERSION}" sh -s - server --flannel-iface eth1

	K3S_TOKEN_VALUE=$(cat /var/lib/rancher/k3s/server/node-token)
	echo "curl -sSL https://get.k3s.io | INSTALL_K3S_VERSION=\"${K3S_VERSION}\" K3S_URL=\"https://${BOX_IP}:6443\" K3S_TOKEN=\"${K3S_TOKEN_VALUE}\" sh -" > /home/vagrant/add_client_command.txt
	chown vagrant:vagrant /home/vagrant/add_client_command.txt
	chmod 444 /home/vagrant/add_client_command.txt
	echo "Add client:"
	cat /home/vagrant/add_client_command.txt

	mkdir -p /vagrant/data
	cp /etc/rancher/k3s/k3s.yaml /vagrant/data
	chown vagrant:vagrant /vagrant/data/k3s.yaml
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|  
	config.vm.box = "generic/debian10"
	config.vm.hostname = BOX_HOSTNAME
	config.vm.network :forwarded_port, guest: 6443, host: 6443
	config.vm.network :public_network, ip: BOX_IP_ADDRESS
	config.vm.provider :virtualbox do |vb|
		vb.memory = BOX_MEMORY_SIZE
		vb.cpus = BOX_CPU_COUNT
	end

	#config.vm.synced_folder ".", "/vagrant", type: "rsync", rsync__exclude: ".git/"
	config.vm.synced_folder ".", "/vagrant"
	config.vm.provision :shell do |s|
		s.inline = $script
		s.args   = [K3S_VERSION, BOX_IP_ADDRESS]
	end
end
